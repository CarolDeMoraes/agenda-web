<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bem-vindo ao Sistema</title>

<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Roboto');
	
	*{
	font-family:"Roboto";
	}
	
	h1{font-size: 200%;
		margin-bottom: 0px;
	}
	
	a{
		text-decoration: none;
		color:#5faae8;
		transition: color .6s;
		
	}
	
	a:hover{
		color:#0166ba;
	}
	
	img{
	display: block;
    margin-left: auto;
    margin-right: auto;
    width: 20%;
	}
</style>



</head>
<body>

	<a href="bemvindo.jsp">
	<img alt="agenda" src="imagens/supimpa.png">
	</a>
	
	<%--coment�rio jsp --%>
	<%--declara uma vari�vel do tipo string --%>
	<%String mensagem= "Bem vindo(a) ao sistema de agenda!!"; %>
	
	<%--somente � necess�rio usar o ; quando estiver declarando vari�veis --%>
	
	<%--escreve a mensagem --%>
	<center>
	<h1><%=mensagem %></h1>
	<a href="mvc?logica=AdicionaContatoLogica">
		<h3>Adicione um contato</h3>
	</a>
	
	<a href="mvc?logica=ListaContatosLogica">
		<h3>Lista de contatos</h3>
	</a>
	<br>
	<%--imprime hora e data atuais --%>
	<br>
	<%= new SimpleDateFormat("dd/MM/yyy").format(new Date()) %>
	<br>
	<%=new SimpleDateFormat("hh:mm:ss").format(new Date()) %>
	</center>
	
</body>
</html>