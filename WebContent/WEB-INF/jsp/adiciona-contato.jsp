<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="prefixo" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="senai" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/jquery-ui.min.css"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<title>Agenda - Cadastro de Contatos</title>
 <style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Roboto');

body{
font-family:"Roboto","Arial";

}

h1{
font-size: 200%;
margin-bottom: 0px;
}

input{
width:150px;
transition: ease-in-out, width .35s ease-in-out;
}

input:focus{
width:10%;
}

.voce{
width: 80px;
height: 30px;
background-color:#1493ce;
border: none;
text-align: center;
color: white;
border-radius: 2.5px;
}


</style>

</head>
<body>
<center>
<prefixo:import url="cabecalho.jsp"/>

<h1>Cadastre</h1>
	<form action="mvc?logica=SalvaContatoLogica" method="post"> <br  /> <br  />
	Nome:
	<input type="text" name="nome" /> <br  /> <br  />
	Email:
	<input type="text" name="email" /> <br  /> <br  />
	Endereço:
	<input type="text" name="endereco" /> <br  /> <br  />
	Data de Nascimento:
	 <senai:campoData id="dataNascimento"/><br  /> <br  />
	<input class="voce" type="submit" value="Salvar" />
	</form>
<prefixo:import url="rodape.jsp"></prefixo:import>
</center>
</body>
</html>