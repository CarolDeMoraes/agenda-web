<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--cabeçalho da taglib core --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="prefixo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8-">
<title>Lista de contatos</title>
<style>
	@import url('https://fonts.googleapis.com/css?family=Roboto');


	*{
	text-align: center;
	font-family:"Roboto";	
	}
	
	table{
	margin-left:auto;
	margin-right:auto;
	}
	
	.links{
		text-decoration:none;
		color:black;
		transition: color .2s;
		padding-left: 5px;
		padding-right: 5px;
	}
	
	.links:hover{
		color: red;
	}
	
	.atual:hover{
		color:#f44280;
		
	}
	
</style>
</head>
<body>
<%--importando página de fora- no caso o cabeçalho --%>
	<prefixo:import url="../../cabecalho.jsp"></prefixo:import>
	
		
	<table border="1" style="border-collapse: collapse; ">
	
		<tr>
			<th>Nº</th>
			<th>Nome</th>
			<th>E-mail</th>
			<th>Endereço</th>
			<th>Data de nascimento</th>
			<th colspan="2">Opções</th>
		</tr>		
	
	
		<prefixo:forEach var="contato" items="${contatos}" varStatus="id">
				<tr bgcolor= "#${id.count%2 == 0 ? '29c0db' : 'fffffffffffffffffff'}">
					<td>${id.count}</td>
					<td>${contato.nome }</td>
						<td>
							<prefixo:if test= "${not empty contato.email }">
							<a href="mailto:${contato.email}">
								${contato.email }
							</a>
							</prefixo:if>
							
							<prefixo:if test= "${empty contato.email }">
							E-mail não informado
							</prefixo:if>
						</td>
						
					<td>${contato.endereco }</td>
					<td>
						<fmt:formatDate value="${contato.dataNascimento.time }" pattern="dd/MM/yyyy"/>
					</td>
					<td>
						<a class="links" href="mvc?logica=RemoveContatoLogica&id=${contato.id}">Excluir</a><a class="links atual" href="mvc?logica=ExibeContatoLogica&id=${contato.id}">Atualizar</a>
					</td>
				</tr>
		</prefixo:forEach>
	</table>	
	<prefixo:import url="../../rodape.jsp"></prefixo:import>
</body>
</html>