<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sucesso...</title>
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Roboto');
	
*{
	font-family:"Roboto";
}

a{
	text-decoration: none;
	color:#5faae8;
	transition: color .6s;
		
	}
	
a:hover{
	color:#0166ba;
	}
	
	
</style>
</head>
<body>

	<h1>Contato ${param.nome }
	adicionado com sucesso!<h1>
	
	<a href="bemvindo.jsp">
		<p>Retorne à página inicial</p>
	</a>
	
</body>
</html>