package br.senai.sp.informatica.agenda.mvc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.mvc.logica.Logica;

//localhost:8080/mvc?
@WebServlet("/mvc")
public class ControllerServlet extends HttpServlet{

		@Override
		protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
			
			
			
			//obt�m o par�metro l�gica da requisi��o
			String parametro = req.getParameter("logica");
			
			//comp�e o nome da classe de l�gica utilizando o par�metro
			//br.senai.sp.informatica.agenda.mvc.logica.xpto
			String className ="br.senai.sp.informatica.agenda.mvc.logica."+ parametro;
			
			try {
				//carrega a classe para a mem�ria
				Class classe= Class.forName(className);
				
				//cria uma nova inst�ncia da classe de l�gica que est� na mem�ria
				Logica logica = (Logica)classe.newInstance();
				
				//executa o m�todo da classe l�gica que foi passada
				//xpto.executa(req,res)
				//o m�todo executa deve retornar um jsp, e o dispatcher deve encaminhar
				//o usu�rio para esse jsp
				String pagina = logica.executa(req, res);
				
				/*obt�m o request dispatcher passando
				 * para ele a p�gina retornada do m�todo
				 *executa e encaminha os usu�rios
				 */
				
				req.getRequestDispatcher(pagina).forward(req, res);
			}catch (Exception e) {
				throw new ServletException("A l�gica de neg�cios causou uma exce��o: ", e);
			}
			
			
		}
}
