package br.senai.sp.informatica.agenda.mvc.logica;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

public class ExibeContatoLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		//cria uma vari�vel do tipo Contato
		Contato contato = null;
		
		//recupera o id por par�metro
		long id  = Long.parseLong(req.getParameter("id"));
		
		//TODO Mover para Dao
		 List<Contato> contatos = new ContatoDao().getLista();
		 
		 //percorre a lista de contatos
		 //tentando achar uma coincid�ncia com o id de par�metro
		 
		 for (Contato c : contatos) {
			if(c.getId() == id) {
				contato =c;
				break;
			}
		}
		 
		 //associa os atributos do contato � atributos da sess�o (request)
		 req.setAttribute("id", contato.getId());
		 req.setAttribute("nome", contato.getNome());
		 req.setAttribute("endereco", contato.getEndereco());
		 req.setAttribute("email", contato.getEmail());
		 
		 //converte a data em formato pt br e associa
		 String dataBr = new SimpleDateFormat("dd/MM/yyyy").format(contato.getDataNascimento().getTime());
		 
		 //associa a data de nascimento � uma vari�vel de sess�o
		 req.setAttribute("dataNascimento", dataBr);
		 
		 //exibe o form preenchido com os dados
		 return "WEB-INF/jsp/exibe-contato.jsp";
		 
	}

}
