package br.senai.sp.informatica.agenda.mvc.logica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

public class SalvaContatoLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		//obt�m os dados do form por meio da request
		String nome = req.getParameter("nome");
		String endereco = req.getParameter("endereco");
		String email = req.getParameter("email");
		String dataEmTexto = req.getParameter("dataNascimento");
		
		
		Calendar dataNascimento = null;
		try {
			//cria uma inst�ncia de java.util.Date
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataEmTexto);
			
			//cria uma inst�ncia de calendar
			dataNascimento = Calendar.getInstance();
		} catch (ParseException e) {
			//exibe os detalhes do erro no console
			e.printStackTrace();
		}
	
		//cria uma inst�ncia de Contato
		Contato contato = new Contato();
		
		//atribui os valores do form ao contato
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setEndereco(endereco);
		contato.setDataNascimento(dataNascimento);
		
		//cria uma inst�ncia de contatoDao
		ContatoDao dao = new ContatoDao();
		
		//salva o contato no banco de dados
		dao.salva(contato);
		
		//exibe a p�gina de feedback para o usu�rio 
		//obs.: voc� pode exibir qualquer outra p�gina, inclusive a lista de contatos
		return "WEB-INF/jsp/contato-adicionado.jsp";
	}

}
