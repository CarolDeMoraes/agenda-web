package br.senai.sp.informatica.agenda.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Logica {
	/**
	 * Este m�todo retorna uma p�gina jsp para que o dispatcher
	 * fa�a o encaminhamento 
	 * 
	 * @param req requisi��o do usu�rio
	 * @param res resposta do servidor
	 * @return p�gina jsp para ser exibida
	 * @return p�gina jsp para ser exibida 
	 * @throws Exception erros que podem ocorrer
	 */	
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception;

	}

