package br.senai.sp.informatica.agenda.mvc.logica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

public class AtualizaContatoLogica implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		//recuperar o id
		long id = Long.parseLong(req.getParameter("id"));
		
		//recupera os dados do form
		String nome = req.getParameter("nome");
		String endereco = req.getParameter("endereco");
		String email = req.getParameter("email");
		String dataEmTexto = req.getParameter("dataNascimento");
		
		//converte a data de nascimento de String pra Calendar
		Calendar dataNascimento = null;

		
		try {
		//converte de String pra java.util.Date	
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataEmTexto);
			//inicalizar o calend�rio
			dataNascimento = Calendar.getInstance();
			dataNascimento.setTime(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//obt�m uma inst�ncia de contato
		Contato contato = new Contato();
		//inicializa os atributos de contato
		contato.setId(id);
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setEndereco(endereco);
		contato.setDataNascimento(dataNascimento);
		
		//obt�m uma inst�ncia de contato dao
		ContatoDao dao = new ContatoDao();
		
		//salva o contato
		dao.salva(contato);
		
		//mostra a lista de contatos
		return "mvc?logica=ListaContatosLogica";
		
	}

}
