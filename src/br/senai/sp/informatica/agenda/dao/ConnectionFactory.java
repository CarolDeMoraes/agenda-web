package br.senai.sp.informatica.agenda.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() {

		try {
			// registra o driver jdbc (para fazer a ponte de conex�o entre server e java)
			Class.forName("com.mysql.jdbc.Driver");

			// retorna um objeto do tipo java.sql.Connection
			return DriverManager.getConnection("jdbc:mysql://localhost/agendam", "root", "root132");
		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
