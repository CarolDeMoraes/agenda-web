package br.senai.sp.informatica.agenda.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.senai.sp.informatica.agenda.model.Contato;

public class ContatoDao {

	// atributos
	Connection connection;
		
	// construtor
	public ContatoDao() {
		// abre uma conex�o com o banco de dados
		this.connection = new ConnectionFactory().getConnection();
	}
	
	// m�todo salva
	public void salva(Contato contato) {
		
		String sql = null;
		
		if (contato.getId() !=null) {
			//se o contato possui um id portano fa�a um update
			sql="UPDATE contato SET nome = ?, email = ?, endereco = ?, dataNascimento =? WHERE id = ?";
			
		}else {
			//se o comando n�o possui portanto fa�a um insert
			// comando sql
			sql = "INSERT INTO contato"
					+ "(nome, email, endereco, dataNascimento)"
					+ "VALUES (?,?,?,?)";
	}

		try {
		// cria um PreparedStatement com o comando sql
		// java.sql.PreparedStatement 
		PreparedStatement stmt = connection.prepareStatement(sql);
		
		// cria os par�metros para as "?"
		stmt.setString(1, contato.getNome());
		stmt.setString(2, contato.getEmail());
		stmt.setString(3, contato.getEndereco());
		
		stmt.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
		
		if(contato.getId() !=null) {
		stmt.setLong(5, contato.getId());
		}
		// executa a instru��o sql
		stmt.execute();
		
		// libera o recurso de prepared statement
		stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
	
				try {
					connection.close();
				} catch (SQLException e) {
	
					throw new RuntimeException(e);
				}
			
		}
	} // fim do m�todo salva

	
	
	public void excluir(Contato contato) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM contato WHERE id = ?");
			stmt.setLong(1, contato.getId());
			stmt.execute();
			stmt.close();
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	
	// m�todo getLista
	public List<Contato> getLista(){
		
		try {
		// cria uma lista de contatos
		List<Contato> contatos = new ArrayList<>();
		
		//jsql
		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM contato");
		
		// guarda os resultado da consulta em um ResultSet(j,sql)
		ResultSet rs = stmt.executeQuery();
		
		// enquanto houver um pr�ximo registro de resultset
		while(rs.next()){
			// cria um novo contato com os dados do resultset
			Contato contato = new Contato();
			contato.setId(rs.getLong("id"));
			contato.setNome(rs.getString("nome"));
			contato.setEmail(rs.getString("email"));
			contato.setEndereco(rs.getString("endereco"));
			
			// Trata a data de nascimento
			// obt�m uma inst�ncia de Calendar
			Calendar data = Calendar.getInstance();
			
			// define a data do calendar com os dados do resultset
			data.setTime(rs.getDate("dataNascimento"));
			contato.setDataNascimento(data);
			
			// adiciona o contato ao arrayList contatos
			contatos.add(contato);
		}// fim do while
		
		// libera o recurso de resultset
		rs.close();
		
		// libera o preraredStatement
		stmt.close();
		
		// retorna a lista de contatos
		return contatos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	} // fim do m�todo getLista

} // fim da classe
