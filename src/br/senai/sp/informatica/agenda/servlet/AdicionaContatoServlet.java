package br.senai.sp.informatica.agenda.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

@WebServlet("/adicionaContato")
public class AdicionaContatoServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// obt�m um PrintWritter
		// esse objeto nos permite escrever mensagens no response
		PrintWriter out = res.getWriter();

		// pega os par�metros da requisi��o
		// por meio dos names no form
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String endereco = req.getParameter("endereco");
		String dataNascimento = req.getParameter("dataNascimento");

		// cria um objeto calend�rio e define como null
		Calendar data = null;
		try {

			// converte a data de String para Date
			// (java.util)
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento);

			// obt�m uma inst�ncia de Calendar
			data = Calendar.getInstance();

			// converter Date para Calendar
			data.setTime(date);
		} catch (ParseException e) {

			// escreve um erro no responde(devolve um erro para o usu�rio)
			out.println("Erro de convers�o de data !");
			return; // interrompe a execu��o do m�todo serivce()
		} // fim do try..catch
		
		// cria uma nova inst�ncia de contato
		Contato contato = new Contato();
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setEndereco(endereco);
		contato.setDataNascimento(data);
		
		// obt�m uma inst�ncia de dao
		ContatoDao dao = new ContatoDao();
		
		// salva o contato
		dao.salva(contato);
		
		
		//feedback para o usu�rio
		//cria um request dispatcher
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/contato-adicionado.jsp");
		
		//encaminha o usu�rio pra essa p�gina
		
		dispatcher.forward(req, res);
		
		
		
		
		// devolve uma reposta ao usu�rio
		/*out.println("<html>");
		out.println("<body>");
		out.println("Contato " + contato.getNome() + " salvo com sucesso");
		out.println("</body>");
		out.println("</html");
		*/
		
	}// fim do m�todo service
} // fim da classe
